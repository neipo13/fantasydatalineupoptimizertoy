from pydfs_lineup_optimizer import Site, Sport, get_optimizer, format_settings
# from datetime import datetime

# startime = datetime.now()

settings = format_settings("{budget:50000,positions:['P','P','C','1B','2B','3B','SS','OF','OF','OF']}")
optimizer = get_optimizer(settings)
optimizer.load_players_from_fantasydata_json("")
# fdid | draftableid | lastname(actually full name) | roster slots(pipe delim) | salary | FFPG (projection) | Locked | Team
for player in optimizer.players:
    print(player)
lineup_generator = optimizer.optimize(10)
for lineup in lineup_generator:
    print(lineup)
# print(datetime.now() - startime)