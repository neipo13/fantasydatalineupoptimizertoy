from collections import namedtuple
import json


LineupPosition = namedtuple('LineupPosition', ['name', 'positions'])


class BaseSettings(object):
    site = None
    sport = None
    budget = 0
    positions = []
    max_from_one_team = None

    @classmethod
    def get_total_players(cls):
        return len(cls.positions)

    @classmethod
    def set_from_json(cls, data):
        temp = json.loads(data)
        cls.site = temp.site
        cls.sport = temp.sport
        cls.budget = temp.budget
        cls.positions = temp.positions
        cls.max_from_one_team = temp.max_from_one_team

